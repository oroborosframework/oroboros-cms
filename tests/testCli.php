<?php
require_once realpath(dirname(__FILE__) . '/../oroboros/src/lib/common/common.php');
if (\oroboros\src\lib\common\is_cli_mode_on()) {
    try {
        //do shell stuff here...
        exec("clear" . PHP_EOL);
        fwrite(STDOUT, "Please enter your name" . PHP_EOL);
        $name = fgets(STDIN);
        fwrite(STDOUT, "Welcome $name");
    } catch (\Exception $e) {

        //report the error to [stderr]
        $fe = fopen('php://stderr', 'w');
        fwrite($fe, $e->getMessage());
        fclose($fe);
    }

    //exit safely
    exit();
} else {
    //do website stuff normally here...
    echo "This is web output.<br>";
}