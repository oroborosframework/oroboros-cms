## Links of Interest ##

Below is a list of links to external resources that you might like to take a look at.

---

### Oroboros Documentation ###

@TODO

---

### Oroboros Components ###

Here are some of our other projects. They can be used in conjunction with this system, or as standalone components.

* [OroborosJS](https://bitbucket.org/oroborosframework/oroborosjs-v-0.0.1) - A lightweight frontend javascript package that supports embedded serverside scripting.
* [OroborosCSS](https://bitbucket.org/oroborosframework/oroboroscss-core-v-0.0.1) - A lightweight responsive css framework that supports embedded scripting.

---

### Dependency Documentation ###

@TODO

---

### Development Resources ###

@TODO

---

