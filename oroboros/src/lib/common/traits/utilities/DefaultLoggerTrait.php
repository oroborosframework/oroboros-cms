<?php

namespace oroboros\src\lib\common\traits\utilities;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Default Logging Behavior Trait>
 * This trait defines the default logging behavior 
 * that is universal to named classtypes
 * (any Oroboros class with self::CLASS_TYPE defined).
 * 
 * All named classtypes have this behavior by default, 
 * but may have overruled this behavior with custom 
 * functionality.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.2a
 */
trait DefaultLoggerTrait {

    /** @var LoggerInterface */
    private $_DefaultLoggerTrait_logger;

    /**
     * Sets a logger.
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger) {
        $this->_DefaultLoggerTrait_logger = $logger;
    }

    /**
     * Gets a logger.
     *
     * @param LoggerInterface $logger
     */
    protected function _getLogger() {
        return $this->_DefaultLoggerTrait_logger;
    }

    protected function _log($level, $message, array $context = array()) {
        if (!isset($this->_DefaultLoggerTrait_logger)) {
            \oroboros\Oroboros::log($level, $message, $context);
        } else {
            $this->_DefaultLoggerTrait_logger->log($level, $message, $context);
        }
    }

}
