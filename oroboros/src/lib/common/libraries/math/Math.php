<?php
namespace oroboros\src\lib\common\libraries\math;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Common Math Library>
 * Performs mathmatical functions.
 * @api \oroboros\src\lib\common\libraries\math\MathInterface
 * @see \oroboros\src\lib\common\libraries\math\MathInterface
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.1a
 */
class Math implements \oroboros\src\lib\common\libraries\math\MathInterface {
    
    const API = "\\oroboros\\src\\lib\\common\\libraries\\math\\MathInterface";
    
    public static function square($int) {
        return $int * $int;
    }

    public static function cube($int) {
        return $int * $int * $int;
    }

    public static function powerof($int, $power) {
        $calc = $int;
        for ($i = 0; $i < $power; $i++) {
            $calc = $calc * $int;
        }
        return $calc;
    }

}
