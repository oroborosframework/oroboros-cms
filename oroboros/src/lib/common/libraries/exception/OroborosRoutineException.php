<?php
namespace oroboros\src\lib\common\libraries\exception;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Routine Exception>
 * This exception designates a problem running a procedural routine within the 
 * Oroboros System. Users should not throw this exception themselves, it is 
 * already recognized where applicable. You SHOULD catch this exception if you 
 * are using the oroboros_run_routine() function.
 * 
 * It is a simple accessor concrete for \OroborosExceptionAbstract. This behaves
 * exactly like a standard exception, but is used to determine that the issue 
 * arose somewhere in the accepted codebase of Oroboros.
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.1a
 */
final class OroborosRoutineException extends OroborosExceptionAbstract {}
