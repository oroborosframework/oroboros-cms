<?php
namespace oroboros\src\lib\common\interfaces\api;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Default API>
 * This class provides a null API if another API is 
 * not attached to an evaluated class. 
 * 
 * Do not attach this to your class, It is already 
 * used if [\Classname::API === FALSE], which is the 
 * default defined in the base class.
 * 
 * @note API Interfaces MUST declare the following constants: [DOCS, NAME, SDK, REPO, VERSION, AUTHOR, LICENSE, SITE]
 * @note API Interfaces MAY declare public methods
 * @note API Interfaces that define public methods will have those methods automatically listed in their API Info when [\oroboros\Oroboros::codex()->api([$instanceOfClass|(string) $validClassname]);] is used
 * @see \oroboros\src\lib\common\abstracts\OroborosBaseAbstract
 * @note Classes that attach API interfaces directly MUST use the [final] keyword in their declaration (eg: final class Foo implements \SomeAPIInterface {}). This is because constants declared in classes can be overruled by child classes, but constants declared in interfaces cause a whitescreen fatal error if overruling is attempted in either classes or interfaces that extend or implement the one where they were overruled. This is typically a very difficult bug to track down when a codebase gets large. This requirement prevents any instance of this happening due to a child class implementing a different api interface. For more info on why this restriction is in place: <https://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem>
 * @see \oroboros\Oroboros
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface DefaultAPIInterface extends \oroboros\src\lib\common\interfaces\api\OroborosFlagInterface {
    
    
    /**
     * <Default API Name>
     * ------
     * Generic name provided by default.
     */
    const NAME = "Unnamed API";
    
    /**
     * <Default API SDK>
     * ------
     * No SDK by default.
     */
    const SDK = FALSE;
    
    /**
     * <Default API Author>
     * ------
     * No author by default.
     */
    const AUTHOR = FALSE;
    
    /**
     * <Default API Repository>
     * ------
     * No repo by default.
     */
    const REPO = FALSE;
    
    /**
     * <Default API Website>
     * ------
     * No website by default.
     */
    const SITE = FALSE;
    
    /**
     * <Default API Documentation>
     * ------
     * No documentation by default.
     */
    const DOCS = FALSE;
    
}
