<?php
namespace oroboros\src\lib\common\interfaces\api;

/* 
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros HTTP Status Code Interface>
 * This file provides consistent definitions for HTTP status codes. 
 * This file exposes  HTTP status codes to your class as class constants. 
 * This file will be updated periodically with future releases to adhere
 *  to the current W3C specifications.
 * @note <INCOMPLETE LIST. Finish before building REST engine!>
 */

interface OroborosHTTPStatusCodeAPIInterface extends \oroboros\OroborosInterface {
    
    /**
     * ---------------- Redirection Errors ----------------
     * These status codes determine that the request must
     * be submitted along another channel, or using a
     * different request pattern.
     * ----------------------------------------------------
     */
    
    /**
     * 305 Use Proxy
     */
    const STATUS_305 = 305;
    const STATUS_305_TITLE = 'Use Proxy';
    const HTTP_USE_PROXY = self::STATUS_305;
    
    /**
     * ---------------- Client Errors ----------------
     * These status codes determine that there was
     * a problem with the client request that does
     * not fit within the specified scope of the
     * application being referenced.
     * -----------------------------------------------
     */
    
    /**
     * 401 Bad Request
     */
    const STATUS_401 = 401;
    const STATUS_401_TITLE = 'Bad Request';
    const HTTP_BAD_REQUEST = self::STATUS_401;
    
    /**
     * 402 Payment Required
     */
    const STATUS_402 = 402;
    const STATUS_402_TITLE = 'Payment Required';
    const HTTP_PAYMENT_REQUIRED = self::STATUS_402;
    
    /**
     * 403 Forbidden
     */
    const STATUS_403 = 403;
    const STATUS_403_TITLE = 'Forbidden';
    const HTTP_FORBIDDEN = self::STATUS_403;
    
    /**
     * 404 Not Found
     */
    const STATUS_404 = 404;
    const STATUS_404_TITLE = 'Not Found';
    const HTTP_NOT_FOUND = self::STATUS_404;
    
    /**
     * 405 Method Not Allowed
     */
    const STATUS_405 = 405;
    const STATUS_405_TITLE = 'Method Not Allowed';
    const HTTP_METHOD_NOT_ALLOWED = self::STATUS_405;
    
    /**
     * 406 Not Acceptable
     */
    const STATUS_406 = 406;
    const STATUS_406_TITLE = 'Not Acceptable';
    const HTTP_NOT_ACCEPTABLE = self::STATUS_406;
    
    /**
     * 407 Proxy Authentication Required
     */
    const STATUS_407 = 407;
    const STATUS_407_TITLE = 'Proxy Authentication Required';
    const HTTP_PROXY_AUTHENTICATION_REQUIRED = self::STATUS_407;
    
    /**
     * 411 Length Required
     */
    const STATUS_411 = 411;
    const STATUS_411_TITLE = 'Length Required';
    const HTTP_LENGTH_REQUIRED = self::STATUS_411;
    
    /**
     * 412 Precondition Failed
     */
    const STATUS_412 = 412;
    const STATUS_412_TITLE = 'Precondition Failed';
    const HTTP_PRECONDITION_FAILED = self::STATUS_412;
    
    /**
     * 413 Request Entity Too Large
     */
    const STATUS_413 = 413;
    const STATUS_413_TITLE = 'Request Entity Too Large';
    const HTTP_REQUEST_ENTITY_TOO_LARGE = self::STATUS_413;
    
    /**
     * 414 Request-URI Too Long
     */
    const STATUS_414 = 414;
    const STATUS_414_TITLE = 'Request-URI Too Long';
    const HTTP_REQUEST_URI_TOO_LONG = self::STATUS_414;
    
    /**
     * 415 Unsupported Media Type
     */
    const STATUS_415 = 415;
    const STATUS_415_TITLE = 'Unsupported Media Type';
    const HTTP_UNSUPPORTED_MEDIA_TYPE = self::STATUS_415;
    
    /**
     * 416 Requested Range Not Satisfiable
     */
    const STATUS_416 = 416;
    const STATUS_416_TITLE = 'Requested Range Not Satisfiable';
    const HTTP_RANGE_REQUEST_NOT_SATISFIABLE = self::STATUS_416;
    
    /**
     * 417 Expectation Failed
     */
    const STATUS_417 = 417;
    const STATUS_417_TITLE = 'Expectation Failed';
    const STATUS_EXPECTATION_FAILED = self::STATUS_417;
    
    
    /**
     * ---------------- Server Errors ----------------
     * These status codes determine that a problem
     * exists with the server, or it's capacity
     * to fulfill the specified request.
     * -----------------------------------------------
     */
    
    /**
     * 500 Server Error
     */
    const STATUS_500 = 500;
    const STATUS_500_TITLE = 'Server Error';
    const HTTP_SERVER_ERROR = self::STATUS_500;
    
    /**
     * 501 Not Implemented
     */
    const STATUS_501 = 501;
    const STATUS_501_TITLE = 'Not Implemented';
    const HTTP_NOT_IMPLEMENTED = self::STATUS_501;
    
    /**
     * 502 Bad Gateway
     */
    const STATUS_502 = 502;
    const STATUS_502_TITLE = 'Bad Gateway';
    const HTTP_BAD_GATEWAY = self::STATUS_502;
    
    /**
     * 503 Service Unavailable
     */
    const STATUS_503 = 503;
    const STATUS_503_TITLE = 'Service Unavailable';
    const HTTP_SERVICE_UNAVAILABLE = self::STATUS_503;
    
    /**
     * 504 Gateway Timeout
     */
    const STATUS_504 = 504;
    const STATUS_504_TITLE = 'Gateway Timeout';
    const HTTP_GATEWAY_TIMEOUT = self::STATUS_504;
    
    /**
     * 505 HTTP Version Not Supported
     */
    const STATUS_505 = 505;
    const STATUS_505_TITLE = 'HTTP Version Not Supported';
    const HTTP_VERSION_NOT_SUPPORTED = self::STATUS_505;
    
}