<?php
namespace oroboros\src\lib\common\interfaces\api;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Exception Code API>
 * This interface declares all of the exception codes used by the system. It may 
 * be implemented to provide easy access to error code levels for checking or throwing 
 * system compliant exceptions.
 * -------------------------------
 * If you throwing an exception from a trait, 
 * you can reference these definitions like so:
 * 
 * throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Some error", \oroboros\src\lib\common\api::EXCEPTION_CODE_GENERAL);
 * 
 * this will insure that all trait exceptions are handled seamlessly, 
 * even if changes to the exception codes themselves happen in the future. 
 * If you absolutely must not type that much, put a use/as statement at 
 * the head of your file like so:
 * 
 */
interface OroborosExceptionCodeInterface extends \oroboros\OroborosInterface {
    
    /**
     * This is the default \Exception code, 
     * so it maps to unknown error. The system will treat all
     * uncaught exceptions with status code 0 as FATAL ERRORS.
     */
    const ERROR_UNKNOWN = 0;
    
    /**
     * <NOTE>
     * Range 1-9999 is reserved for HTTP Status codes.
     * They don't go that high, but the spec may change 
     * in the future, so there is room for error to avoid
     * possibly having to refactor if W3C changes spec to
     * include a higher range.
     * @see <\oroboros\src\lib\common\api\OroborosHTTPStatusCodeAPIInterface>
     */
    
    /**
     * <Safe 3rd Party Code Range>
     * Oroboros will never claim an error code above this number. 
     * If you want to extend this API with your own codes, they should be
     * higher than this number to avoid system collisions, which might produce 
     * really strange bugs if you are also using the system provided exceptions.
     */
    const SAFE_MINIMUM_ERROR_RANGE = 30000;
    
    const ERROR_CORE = 10000; //Generic Oroboros Core Error - Covers errors that are known to have arisen from the system, but not really clear otherwise. Usually treated as FATAL ERRORS if not caught.
    const ERROR_SYNTAX = 11000; //Syntax Error - Very sloppy programming somewhere in your codebase. If you use an IDE, look for the error icon. Otherwise check the log.
    const ERROR_FILESYSTEM = 12000; //Filesystem Error - Any issues arising from file access, missing resources, etc.
    const ERROR_DATABASE = 13000; //Database Error - A database error. These can range from trivial to very, very bad. Check the log if you see these.
    const ERROR_SESSION = 14000; //Session Error - A session mismatch. May occur either due to the server or the client. Usually aggravating, but not serious.
    const ERROR_SECURITY = 15000; //Security Error - An error with a security protocol. Usually thrown because of an intentional, malicious attempt to break things by a 3rd party.
    const ERROR_SECURITY_LOCKED_RESOURCE = 15100; //Security Error - An attempt was made to access a locked resource in the codebase.
    const ERROR_SECURITY_LOCKED_FILE = 15110; //Security Error - An attempt was made to access a locked resource in the filebase.
    const ERROR_SECURITY_LOCKED_TABLE = 15120; //Security Error - An attempt was made to access a locked table in the database.
    const ERROR_SECURITY_LOCKED_COLUMN = 15121; //Security Error - An attempt was made to access a locked column in the database.
    const ERROR_ROUTING = 16000; //Routing Error - An error with DNS resolution. This means a request was made for something that doesn't exist, or that resource pointers are wrong.
    const ERROR_LOGIC = 17000; //Logic Error - An error generated from sloppy programming logic, like infinite recursion or dividing by zero. These should usually be anticipated and handled before they happen, and indicate that a patch is needed.
    const ERROR_LOGIC_BAD_PARAMETERS = 17001; //Logic Error - A parameter was supplied, but is malformed.
    const ERROR_LOGIC_MISSING_PARAMETERS = 17002; //Logic Error - A required parameter was not supplied.
    const ERROR_INITIALIZATION = 18000; //Initialization Error - An error with initialization/bootstrap. These are bad, but usually fixeable.
    const ERROR_INSTALLATION = 19000; //Installation Error - An error with initial setup. These are very bad.
    const ERROR_MODEL = 20000; //Model Error - Any error that cannot be handled by a model. These are bad.
    const ERROR_VIEW = 21000; //View Error - Any error in output logic. This means that your backend did it's job, but it can't display that it did to you.
    const ERROR_CONTROLLER = 22000; //Controller Error - An error in the controller, often due to bad client input and usually not very severe.
    const ERROR_ADAPTER = 23000; //Adapter Error - Any error with an adapter used for interfacing with other languages or server utilities (eg: git, bash, cron, python, etc)
    const ERROR_MODULE = 24000; //Module Error - Any error from a 3rd party module
    const ERROR_ROUTINE = 25000; //Routine Error - An error generated while running a procedural routine.
    const ERROR_SDK = 26000; //SDK Error - Any error generated from a remote 3rd party service returning bad results. May or may not be fixable, depending on the nature of the service.
    const ERROR_NETWORK = 27000; //RESERVED FOR EXPANSION
    const ERROR_CLUSTER = 28000; //RESERVED FOR EXPANSION
    const ERROR_NODE = 29000; //RESERVED FOR EXPANSION
    
}