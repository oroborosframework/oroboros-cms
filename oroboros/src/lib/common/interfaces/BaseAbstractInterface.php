<?php
namespace oroboros\src\lib\common\interfaces;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface BaseAbstractInterface 
extends \oroboros\OroborosInterface, //Basic system definitions
        \oroboros\src\lib\common\interfaces\api\OroborosExceptionCodeInterface, //basic exception code definitions
        \oroboros\src\lib\common\interfaces\api\OroborosFlagInterface, //Basic flag definitions
        \oroboros\src\lib\common\interfaces\psr3\LogLevel //Psr3 standardized log levels
{
    
    /**
     * <Systemwide Default Constructor Method>
     * Constructors SHOULD NOT perform operations directly. 
     * Constructors SHOULD ONLY return a built, non-initialized 
     * object with a unique fingerprint.
     * 
     * Classes in this system MUST provide a constructor 
     * that provides two, and only two parameters, 
     * as defined below. 
     * 
     * These parameters SHOULD be stored in private 
     * class variables, and not be processed until 
     * the initialize method is called. This gives 
     * an opportunity to allow custom settings for 
     * your class, for unit testing and debugging 
     * to be injected into an instance of it that 
     * has been created, to emulate settings where 
     * a bug may be encountered.
     * 
     * Params indicates any arguments you need to pass 
     * into the constructor, which should be designated 
     * as key => value pairs. Flags indicate any additional 
     * constants that should be passed to trigger interal 
     * operations. These should either be universal 
     * Oroboros Flags as defined in the flag interface, 
     * or class constants of the class itself in the 
     * format of [ const FLAG_FLAGNAME = "::somevalue::"; ]. 
     * Any additional arguments needed should be handled 
     * from these two arrays. Both MUST also provide default 
     * empty values, so constructors can be called without 
     * arguments if required. This should immediately throw 
     * an exception in the constructor if that specific class 
     * constructor MUST have an input value.
     * 
     * @param array $params
     * @param array $flags
     * @return void
     * @since 0.0.2a
     */
    public function __construct(array $params = array(), array $flags = array());
    
    /**
     * <Systemwide Default Initialization Method>
     * Classes in this system MUST provide a initialization 
     * method that provides two, and only two parameters, 
     * as defined below. 
     * 
     * These parameters SHOULD be stored in private 
     * class variables before processing, and SHOULD 
     * be able to be supplied either in the constructor 
     * OR the initialize method with no actual difference 
     * in operation (unless initial values are overruled 
     * with non-empty values in the initialize method).
     * the initialize method is called externally. 
     * This gives an opportunity to allow custom 
     * settings for your class, for unit testing 
     * and debugging parameters to be injected into 
     * an instance of it that has been created, to 
     * emulate settings where a bug may be encountered.
     * 
     * Params indicates any arguments you need to pass 
     * into the initialization method, which should be 
     * designated as key => value pairs. Flags indicate any 
     * additional constants that should be passed to trigger 
     * interal operations. These should either be universal 
     * Oroboros Flags as defined in the flag interface, 
     * or class constants of the class itself in the 
     * format of [ const FLAG_FLAGNAME = "::somevalue::"; ]. 
     * Any additional arguments needed should be handled 
     * from these two arrays. Both MUST also provide default 
     * empty values, so constructors can be called without 
     * arguments if required. This should immediately throw 
     * an exception in the initialization method if that 
     * specific class constructor MUST have an input value.
     * 
     * @param array $params (optional)
     * @param array $flags (optional)
     * @return object instanceof self (eg: return $this;)
     * @since 0.0.2a
     */
    public function initialize(array $params = array(), array $flags = array());
    
    /**
     * <Systemwide Runtime Instance Fingerprint>
     * Returns the unique fingerprint of the object instance 
     * created when the base class is constructed. 
     * This method cannot be overrided.
     * @return string
     * @final
     * @since 0.0.2a
     */
    public function fingerprint();
}
