<?php
namespace oroboros\src\lib\common\interfaces\psr3;
/**
 * @author PHP-FIG <http://php-fig.org>
 */
interface LogLevel
{
    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';
}