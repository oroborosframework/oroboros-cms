<?php

namespace oroboros\src\lib\common\abstracts\views;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Abstract View>
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class AbstractView 
extends \oroboros\src\lib\common\abstracts\OroborosBaseAbstract 
implements \oroboros\src\lib\common\interfaces\views\ViewInterface, 
        \oroboros\src\lib\common\interfaces\psr3\LoggerAwareInterface 
{

    use \oroboros\src\lib\common\traits\utilities\LoaderTrait;

    use \oroboros\src\lib\common\traits\utilities\DefaultLoggerTrait;

    const CONTENT_TYPE = 'text/html; charset=UTF-8';
    /**
     * These are the types of resources that can be loaded with the $this->load method
     * @var array $_allowed_loader_types 
     */
    private $_allowed_loader_types = array(
        "library",
        "module",
        "template",
        "theme"
    );
    private $_valid_output_modes = array(
        "plaintext" => self::FLAG_OUTPUT_PLAINTEXT,
        "html" => self::FLAG_OUTPUT_HTML,
        "css" => self::FLAG_OUTPUT_CSS,
        "javascript" => self::FLAG_OUTPUT_JAVASCRIPT,
        "email" => self::FLAG_OUTPUT_EMAIL,
        "rss" => self::FLAG_OUTPUT_RSS,
        "cron" => self::FLAG_OUTPUT_CRON,
        "robots" => self::FLAG_OUTPUT_ROBOTS,
        "sitemap" => self::FLAG_OUTPUT_SITEMAP,
    );
    private $_output_mode;
    private $_headers = array('Content-Type' => self::CONTENT_TYPE);
    private $_meta = array();
    private $_css = array();
    private $_scripts = array();
    private $_fonts = array();
    private $_content = array();
    private $_base_url = OROBOROS_URL;

    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
        $this->_setAllowedLoaderTypes($this->_allowed_loader_types);
        ob_start();
    }

    public function __destruct() {
        parent::__destruct();
        print ob_get_clean();
    }

    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
        $this->_initializeLoader();
        if (!\oroboros\src\lib\common\is_cli()) {
            //web request setup
            $headers = \apache_request_headers();
            $this->_setParam('request_headers', $headers);
        } else {
            //command line setup
            $this->_setParam('request_headers', FALSE);
        }
    }
    
    public function redirect($location, $internal = 1) {
        header("Location: " . (($internal)) ? $this->_toUrl($location) : $location);
    }
    
    public function render(array $params = array(), array $flags = array()) {
        if (!\oroboros\src\lib\common\is_cli()) {
            $this->_processHeaders();
        }
        if ($this->_checkFlag(self::FLAG_TYPE_OPTIONS) || $this->_checkFlag(self::FLAG_TYPE_HEAD)) {
            return;
        }
        $this->_buildContent($params, $flags);
    }
    
    public function reset(array $params = array(), array $flags = array()) {
        $this->_headers = array('Content-Type' => self::CONTENT_TYPE);
        $this->_meta = array();
        $this->_content = array();
        $this->_css = array();
        $this->_scripts = array();
        $this->_fonts = array();
    }
    
    protected function _buildContent(array $params = array(), array $flags = array()) {}
    
    protected function _resetHeaders() {
        $this->_headers = array('Content-Type' => self::CONTENT_TYPE);
    }
    
    protected function _setHeaders(array $headers) {
        foreach ($headers as $title => $value) {
            $this->_headers[$title] = $value;
        }
    }
    
    protected function _processHeaders() {
        foreach ($this->_headers as $title => $value) {
            header($title . '="' . $value . '"');
        }
    }
    
    protected function _toUrl($filepath) {
        $basepath = realpath(dirname(\oroboros\OroborosInterface::OROBOROS_BASEPATH)) . DIRECTORY_SEPARATOR;
        return str_replace($basepath, $this->_base_url . '/', $filepath);
    }
    
    protected function _updateParameterConditions($parameter_key) {
            $name = str_replace(' ', NULL, ucwords(str_replace('-', ' ', $parameter_key)));
            if (method_exists($this, '_reset' .$name)) {
                $this->{'_reset' . $name}();
            }
            if (method_exists($this, '_set' .$name)) {
                $this->{'_set' . $name}($this->_getParam($parameter_key));
            }
    }

}
