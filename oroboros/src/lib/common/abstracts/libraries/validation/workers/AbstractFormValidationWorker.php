<?php
namespace oroboros\src\lib\common\abstracts\libraries\validation\workers;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of AbstractFormValidationWorker
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class AbstractFormValidationWorker
extends \oroboros\src\lib\common\abstracts\libraries\validation\AbstractValidationWorker 
implements \oroboros\src\lib\common\interfaces\api\OroborosRegexAPIInterface 
{

    const WORKER_TYPE = 'form';

    private $_errors = array();
    private $_schema = array();
    private $_verdict = FALSE;
    private $_details = array();
    private $_raw;

    public function validate($data, array $schema) {
        $this->_raw = $data;
        $this->_schema = $schema;
        foreach ($data as $key => $value) {
            $this->_details[$key] = $this->_evaluateFormData($key, $value);
        }
        $result = \oroboros\Common::cast('object', array(
            "valid" => (!(in_array(FALSE, $this->_details)) ? TRUE : FALSE),
            "verdict" => ((in_array(FALSE, $this->_details)) ? $this->_details : TRUE),
            "raw" => $this->_raw,
        ));
        return $result;
    }

    protected function _evaluateFormData($key, $value) {
        if (!isset($this->_schema['required']) || !isset($this->_schema['required'][$key])) {
            $details = TRUE;
        } else {
            $schema = $this->_schema['required'][$key];
            $schema = array_shift($schema);
            $method = '_' . array_search($schema, $this->_schema['required'][$key]);
            $details = $this->{$method}($value, $schema);
        }
        return $details;
    }

    private function _regex($subject, $pattern) {
        $verdict = FALSE;
        $check = function ($reg) {
            if ((strpos($reg, '%') === 0)) {
                return urldecode($reg);
            } elseif (defined("self::" . $reg)) {
                return constant("self::" . $reg);
            } else {
                return $reg;
            }
        };
        $regex = ((is_array($pattern)) ? array_map($check, $pattern) : $check($pattern));
        if (is_array($regex)) {
            $result = array();
            foreach ($regex as $key => $patternmatch) {
                $result[$key] = $this->_regex($subject, $patternmatch);
            }
            $verdict = ((in_array(1, $result) || in_array(TRUE, $result)) ? TRUE : FALSE);
        } else {
            if (urldecode($regex) !== $regex) {
                $pattern = urldecode($regex);
            } elseif (defined('\oroboros\src\lib\common\libraries\regex\Regex::' . $regex)) {
                $pattern = constant('\oroboros\src\lib\common\libraries\regex\Regex::' . $regex);
            }
            $verdict = preg_match($regex, $subject);
        }
        return ($verdict) ? TRUE : FALSE;
    }

    private function _options($subject, $pattern) {
        $verdict = FALSE;
        $verdict = (in_array($subject, $pattern) ? TRUE : FALSE);
        return $verdict;
    }

    private function _match($subject, $pattern) {
        $verdict = TRUE;
//        $verdict = (in_array($subject, $pattern) ? TRUE : FALSE);
        return $verdict;
    }

}

