<?php

namespace oroboros\src\lib\common\abstracts;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of AbstractDataObject
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class AbstractDataObject extends \oroboros\src\lib\common\abstracts\OroborosBaseAbstract implements \oroboros\src\lib\common\interfaces\DataObjectInterface {

    private $_data;
    private $_format;
    private $_valid_formats = array(
        'object',
        'array',
        'serialized',
        'json'
    );
    private $_access_rights = array();

    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
        $this->_init($params, $flags);
    }

    public function __toString() {
        if ($this->_checkLock('read')) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Cannot return requested key [" . $key . "] because the parent object is not readable." . PHP_EOL . '> Thrown at ' . __LINE__ . ' of ' . __METHOD__, self::ERROR_SECURITY_LOCKED_RESOURCE);
        }
        switch ($this->_format) {
            case 'serialized':
                return serialize($this->_data);
                break;
            case 'json':
                return json_encode($this->_data);
                break;
            default:
                return (string) $this->_data;
                break;
        }
    }

    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
        $this->_init($params, $flags);
    }

    public function get($key) {
        if ($this->_checkLock('read')) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Cannot return requested key [" . $key . "] because the parent object is not readable." . PHP_EOL . '> Thrown at ' . __LINE__ . ' of ' . __METHOD__, self::ERROR_SECURITY_LOCKED_RESOURCE);
        }
        if (!array_key_exists($key, $this->_data)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Requested key [" . $key . "] does not exist." . PHP_EOL . '> Thrown at ' . __LINE__ . ' of ' . __METHOD__, self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        return $this->_data[$key];
    }

    public function set($key, $value) {
        if ($this->_checkLock('write')) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Cannot return requested key [" . $key . "] because the parent object is not writeable." . PHP_EOL . '> Thrown at ' . __LINE__ . ' of ' . __METHOD__, self::ERROR_SECURITY_LOCKED_RESOURCE);
        }
        parent::set($key, $value);
    }

    private function _init(array $params = array(), array $flags = array()) {
        if (!empty($flags)) {
            $this->_setFlags($flags);
        }
        $this->_setFlags($flags);
        if (!empty($params)) {
            $this->_setParams($params);
        }
        if (array_key_exists('package', $params)) {
            $this->package($params['package'], ((isset($params['format'])) ? $params['format'] : array()));
        }
        $this->_data = $params;
    }

    public function package(array $data = array(), $format = 'object') {
        if ($this->isInitialized() && $this->_checkLock('write')) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Cannot return requested key [" . $key . "] because the parent object is not writeable." . PHP_EOL . '> Thrown at ' . __LINE__ . ' of ' . __METHOD__, self::ERROR_SECURITY_LOCKED_RESOURCE);
        }
        if (!in_array($format, $this->_valid_formats)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid data format type: [" . (string) $format . "]. Valid data types are [" . implode(', ', $this->_valid_formats) . "]." . PHP_EOL . '> Thrown at ' . __LINE__ . ' of ' . __METHOD__, self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        $this->_data = ((is_object($data)) ? get_object_vars($data) : $data);
    }

    protected function _updateFlagConditions() {
        if ($this->isInitialized()) {
            if ($this->_checkFlag(self::FLAG_READONLY)) {
                $this->_setFlag(self::FLAG_LOCK_WRITE);
                $this->_setFlag(self::FLAG_LOCK_EXECUTE);
            }
            if ($this->_checkFlag(self::FLAG_LOCK_READ)) {
                $this->_setLock("read", TRUE);
            }
            if ($this->_checkFlag(self::FLAG_LOCK_WRITE)) {
                $this->_setLock("write", TRUE);
            }
            if ($this->_checkFlag(self::FLAG_LOCK_EXECUTE)) {
                $this->_setLock("execute", TRUE);
            }
        }
    }

}
