oroboros.ui = {
    progressbar: function (id, steps) {
        var ProgressBarController = function (id, steps) {
            this.id = id;
            this.increment = {
                pointer: 0,
                min: 0,
                max: steps,
                increment: (100 / steps)
            };
            this.update();
            this.data = {
            };
            return ((this instanceof window.constructor)
                    ? new ProgresBarController(id, steps)
                    : this);
        }
        ProgressBarController.prototype = {
            element: function () {
                return this.id;
            },
            update: function () {
                jQuery(this.id).attr("aria-valuenow", this.increment.pointer);
                jQuery(this.id).attr("aria-valuemin", this.increment.min);
                jQuery(this.id).attr("aria-valuemax", this.increment.max);
            },
            increase: function (steps) {
                var diff = Math.round(this.increment.pointer
                        + (this.increment.increment * steps));
                if (diff >= this.increment.max) {
                    this.increment.pointer = this.increment.max;
                } else {
                    this.increment.pointer = (diff);
                }
                this.update();
                this.animate();
                if (this.increment_callback && jQuery.isFunction(this.increment_callback)) {
                    this.increment_callback(this.element(), this.increment, this.data);
                }
                if (this.increment.pointer >= this.increment.max) {
                    if (this.max_callback && jQuery.isFunction(this.max_callback)) {
                        this.max_callback(this.element(), this.increment, this.data);
                    }
                }
            },
            decrease: function (steps) {
                var diff = Math.round(this.increment.pointer - (this.increment.increment * steps));
                if (diff <= this.increment.min) {
                    this.increment.pointer = this.increment.min;
                } else {
                    this.increment.pointer = (diff);
                }
                this.update();
                this.animate();
                if (this.increment_callback && jQuery.isFunction(this.increment_callback)) {
                    this.increment_callback(this.element(), this.increment, this.data);
                }
                if (this.increment.pointer <= this.increment.min) {
                    if (this.min_callback && jQuery.isFunction(this.min_callback)) {
                        this.min_callback(this.element(), this.increment, this.data);
                    }
                }
            },
            animate: function () {
                var val = Math.round(((this.increment.pointer / this.increment.max) * 100));
//                    jQuery(this.id).animate({
//                        width: (val/100)*jQuery(this.id).width(),
//                    });
                jQuery(this.id).width(val + "%");
            },
            complete: function (callback) {
                if (!(jQuery.isFunction(callback))) {
                    throw new Error("Invalid function passed at ");
                }
                this.max_callback = callback;
            },
            empty: function (callback) {
                if (!(jQuery.isFunction(callback))) {
                    throw new Error("");
                }
                this.min_callback = callback;
            },
            change: function (callback) {
                if (!(jQuery.isFunction(callback))) {
                    throw new Error("");
                }
                this.increment_callback = callback;
            }
        }
        return new ProgressBarController(id, steps);
    }
};
window.oroboros.installer = (function () {
    var OroborosInstaller = function (s, a, f) {
        return ((this instanceof window.constructor)
                ? new OroborosInstaller(s, a, f)
                : this);
    };
    OroborosInstaller.prototype.ajax = function (command, args, callback) {
        var data = JSON.stringify({"command": command, "args": args});
        jQuery.ajax({
            url: window.oroboros_core_metadata().app.ajax.url,
            type: "post",
            data: {data: data},
            complete: callback
        });
    };
    OroborosInstaller.prototype.initialize = (function (parent) {
        var OroborosInstallerConstructor = function () {
            if (!(this instanceof window.constructor)) {
                this.initialize();
                return this;
            } else {
                return new OroborosInstallerConstructor(parent);
            }
        }
        OroborosInstallerConstructor.prototype.metaData = function () {
            this.metadata = oroboros_core_metadata();
        }
        OroborosInstallerConstructor.prototype.initializeState = function () {
            this.state = new parent.app.pattern.State({
                initialize: function () {
                },
                update: function () {
                },
                render: function () {
                },
            });
        };
        OroborosInstallerConstructor.prototype.initialize = function () {
            this.metaData();
            this.initializeState();
            this.initialized = true;
            return this;
        };

        return ((this instanceof window.constructor) ? new OroborosInstallerConstructor : this);
    })(oroboros);
    return new OroborosInstaller();
})();

jQuery(document).ready(function () {
    jQuery.noConflict();
    
    //button disable assist
    jQuery('.btn-group .btn.disabled').click(function (event) {
        event.stopPropagation();
        event.preventDefault();
        return false;
    });
    //register unload function
    jQuery(window).on('beforeunload', function () {
        window.oroboros.exit();
    });
    //extension scripts
    //tooltips
    jQuery('[data-toggle="tooltip"]').tooltip();
});