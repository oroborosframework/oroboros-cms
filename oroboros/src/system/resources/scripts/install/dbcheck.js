/* 
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

oroboros.env = {
    currentKey: null,
    taskProgress: null,
    datatable: null,
    taskset: null,
    callbacks: {},
    evaluate: function () {
        oroboros.installer.ajax("dbcheck", {}, function (data, status, xhr) {
            console.log(data.responseText);
        });
    },
    tabulate: function () {
        oroboros.installer.ajax("envtable", {}, function (data, status, xhr) {
            console.info("Tabulation results:");
            if (data) {
                var response = JSON.parse(data.responseText);
            }
            console.dir(response.data);
            var taskcount = 0;
            jQuery.each(response.data, function (key, value) {
                jQuery.each(value.tasks, function (subkey, task) {
                    var markup = '<tr id="' + key + '-' + subkey + '" class="">\n' +
                            '<th scope="row" class="category">' + value.meta.category + '</th>\n' +
                            '<th scope="row" class="taskname">' + task.title + '</th>\n' +
                            '<td class="status">\n' +
                            '<span class="text-muted glyphicon glyphicon-refresh glyphicon-spin"></span></td>\n' +
                            '<td class="diagnosis"><p class="text-muted">checking...</p></td>\n' +
                            '<td class="notes">' +
                            '<p class="pending text-info"><span class="glyphicon glyphicon-time"></span> Awaiting assessment...</p>' +
                            '<p class="fail text-danger hidden"><span class="glyphicon glyphicon-remove"></span> ' + task.message.fail + '</p>' +
                            '<p class="success text-success hidden"><span class="glyphicon glyphicon-ok"></span> ' + task.message.success + '</p>' +
                            '</td>\n' +
                            '</tr>';
                    jQuery('table#environment-results').children('tbody').append(markup);
                    taskcount++;
                    //window.oroboros.env.currentKey = key;
                    jQuery.each(value.tasks, function () {
                        window.oroboros.env.profile(key, subkey, task);
                    });
                    jQuery.each(window.oroboros.env.callbacks, function (i, callback) {
                        delete window.oroboros.env.callbacks[i];
                        callback.execute();
                    });
                });
            });
            window.oroboros.env.taskProgress = new oroboros.ui.progressbar('#environment-progress', taskcount);
            window.oroboros.env.taskProgress.complete(function () {
                jQuery(window.oroboros.env.taskProgress.id).removeClass('progress-bar-striped');
                jQuery(window.oroboros.env.taskProgress.id).removeClass('active');
            });
            //Tabulation complete, safe to add dataTables styles (must be done in AJAX callback)
            this.datatable = jQuery('#environment-results').dataTable({
                initComplete: function (settings) {
                    jQuery('#environment-results_length').append('<span class="text-primary"> (<span class="value-current">0</span> / <span class="value-max">' + taskcount + '</span> completed)</span>');
                }
            });
        });
    },
    profile: function (category, index, details) {
        if (typeof window.oroboros.env.callbacks[category + '-' + index] !== "undefined") {
            return false;
        }
        var callback, EnvironmentCategoryIndex = function (category, index, details) {
            this.category = category;
            this.index = index;
            this.details = details;
            this.execute = function () {
                oroboros.installer.ajax("profile", {
                    category: category,
                    index: index
                }, function (data, status, xhr) {
                    var i, e, response, selector = jQuery('#' + category + '-' + index);
                    try {
                        response = JSON.parse(data.responseText);
                        if (response.status === 200) {
                            selector.addClass('success');
                            selector.children('td.status').children('span.glyphicon').removeClass('glyphicon-spin').removeClass('text-muted').removeClass('glyphicon-refresh').addClass('glyphicon-ok').addClass('text-success');
                            selector.children('td.diagnosis').children('p').removeClass('text-muted').addClass('text-success').text('Pass');
                            selector.children('td.notes').children('p.pending').addClass('hidden');
                            selector.children('td.notes').children('p.success').removeClass('hidden');
                        } else {
                            selector.addClass('danger');
                            selector.children('td.status').children('span.glyphicon').removeClass('glyphicon-spin').removeClass('text-muted').removeClass('glyphicon-refresh').addClass('glyphicon-remove').addClass('text-danger');
                            selector.children('td.diagnosis').children('p').removeClass('text-muted').addClass('text-danger').text('Fail');
                            selector.children('td.notes').children('p.pending').addClass('hidden');
                            selector.children('td.notes').children('p.fail').removeClass('hidden');
                        }
                    } catch (e) {
                        selector.addClass('danger');
                        selector.children('td.status').children('span.glyphicon').removeClass('glyphicon-spin').removeClass('text-muted').removeClass('glyphicon-refresh').addClass('glyphicon-remove').addClass('text-danger');
                        selector.children('td.diagnosis').children('p').removeClass('text-muted').addClass('text-danger').text('Fail');
                        selector.children('td.notes').children('p.pending').addClass('hidden');
                        selector.children('td.notes').children('p.fail').removeClass('hidden');
                        //Error on the server
                        console.error(e);
                        console.warn("Invalid JSON Response. Check server configuration.");
                        console.groupCollapsed("Profile of [" + category + "][" + index + "]");
                        console.log('status');
                        console.log(status);
                        console.log('data');
                        console.log(data.responseText);
                        console.log('xhr');
                        console.log(xhr);
                        console.log('original details');
                        console.log(details);
                        console.groupEnd();
                    }
                    window.oroboros.env.progress(1);
                });
            };
            return ((this === window) ? new EnvironmentCategoryIndex(category, index, details) : this);
        };
        EnvironmentCategoryIndex.prototype.constructor = EnvironmentCategoryIndex;
        EnvironmentCategoryIndex.prototype.metadata = window.oroboros_core_metadata();
        callback = new EnvironmentCategoryIndex(category, index, details);
        console.groupCollapsed('Building [' + category + '][' + index + '] command');
        console.dir(callback);
        console.groupEnd();
        window.oroboros.env.callbacks[category + '-' + index] = callback;
    },
    progress: function ($value) {
        window.oroboros.env.taskProgress.increase($value);
    },
};
function oroboros_debug_log($data) {
    jQuery('#debug_log').append('<span class="debug-log-entry">' + $data + '</span>');
}

jQuery(document).ready(function () {
    jQuery("form[name='database-credentials']").submit(function(event){
        var values, elements = {};
        event.preventDefault();
        event.stopPropagation();
        alert("you submitted the form!");
        elements = jQuery("form[name='database-credentials'] :input").each(function(){
            values[this.name] = jQuery(this).val();
        });
        console.dir(values);
        return false;
    });
//    oroboros.env.tabulate();
//    oroboros.env.evaluate();
});