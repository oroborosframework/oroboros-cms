<?php

namespace oroboros\src\system\index\install\workers;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * 
 */
final class FormWorker 
extends \oroboros\src\lib\common\abstracts\libraries\AbstractWorker 
implements \oroboros\src\lib\common\interfaces\api\OroborosRegexAPIInterface, 
        \oroboros\src\lib\common\interfaces\api\OroborosExceptionCodeInterface, 
        \oroboros\src\lib\common\interfaces\api\OroborosFlagInterface, 
        \oroboros\src\lib\common\interfaces\psr3\LogLevel 
                {

    public function initialize($schema, $conditions = NULL) {
        $valid_states = array_keys(get_object_vars($schema));
        $this->_registerStates($valid_states);
        parent::initialize($schema);
    }

    /**
     * Validates form data based on the current state.
     * @param type $data
     * @return type
     */
    public function validate($data = NULL) {
        $results = array();

//        \dev\Utils::crash(array("Reason for Breakpoint" => "Debugging validation", "Data" => $data));
        foreach ($data as $key => $value) {
            $validation_mode = key(get_object_vars($this->_schema()->required->{$key}));
            switch ($validation_mode) {
                case "options":
                    //supplied value must be in the listed options
                    $results[$key] = ((in_array($value, $this->_schema()->required->{$key}->{$validation_mode})));
                    break;
                case "regex":
                    //perform a pattern match against the supplied value
                    $regexData = $this->_schema()->required->{$key}->{$validation_mode};
                    $results[$key] = ($this->_runValidationRegex($value, $regexData) ? TRUE : FALSE);
                    break;
                default:
                    throw new \OroborosInstallerException("Unknown form validation mode: " . $validation_mode);
                    break;
            }
        }
//        \dev\Utils::crash(array("Reason for Breakpoint" => "Debugging validation"));
        parent::validate($data);
        $verdict = array(
            "verdict" => ((in_array(FALSE, $results)) ? false : true),
            "results" => $results,
            "data" => $data
        );
        \dev\Utils::crash(array("Reason for Breakpoint" => "Debugging validation", "Verdict" => $verdict));
        return $verdict;
    }

    private function _runValidationRegex($datasource, $regex) {
        var_dump($datasource);
        $check = function ($reg) {
            if (is_string($reg)) {return (defined("self::" . $reg) ? constant("self::" . $reg) : urldecode($reg));}
        };
        $regex = ((is_array($regex)) ? array_map($check, $regex) : $check($regex));
        if (is_array($regex)) {
            $result = array();
            foreach ($regex as $key => $pattern) {
                $result[$key] = $this->_runValidationRegex($datasource, $pattern);
            }
            return ((in_array(1, $result) || in_array(TRUE, $result)) ? TRUE : FALSE);
        } elseif (is_string($regex)) {
            if (urldecode($regex) !== $regex) {
                //pattern is urlencoded, decode and apply directly
                $pattern = urldecode($regex);
                $result = preg_match($pattern, $datasource);
            } elseif (defined('\oroboros\src\lib\common\libraries\regex\Regex::' . $regex)) {
                //defined pattern, use \oroboros\lib\Regex definition
                $pattern = constant('\\oroboros\\lib\\Regex::' . $regex);
                $result = preg_match($pattern, $datasource);
            } else {
//                \dev\Utils::crash(array("verdict" => "Unhandled exception", "data" => $regex, "backtrace" => debug_backtrace()));
                throw new \OroborosInstallerException("Unknown regex pattern ["
                . $regex . "] at " . __LINE__ . ' of ' . __METHOD__);
            }
//            \dev\Utils::crash(array("Reason for Breakpoint" => "Debugging validation", "Datasource" => $datasource, "Regex" => $regex));
            return (($result) ? TRUE : FALSE);
        }
    }

    protected function _onStateChange() {
        
    }

}
