-- 
-- Oroboros Core System Installation Database SQL
-- 

---
-- The MIT License
-- 
-- Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
---

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `oroboros`
--

-- --------------------------------------------------------

--
-- Table structure for table `system_applications`
--

CREATE TABLE IF NOT EXISTS `system_applications` (
  `application` varchar(64) CHARACTER SET utf8 NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `author` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `type` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT 'app' COMMENT 'The type of the application',
  `created` datetime NOT NULL,
  `status` varchar(32) CHARACTER SET utf8 DEFAULT 'staging' COMMENT 'The current deployment status of the application',
  `domain` varchar(253) CHARACTER SET utf8 DEFAULT NULL COMMENT 'The domain pointer for the application',
  `signature` binary(60) NOT NULL,
  PRIMARY KEY (`author`,`application`),
  UNIQUE KEY `author_2` (`author`,`title`),
  UNIQUE KEY `signature` (`signature`),
  UNIQUE KEY `domain` (`domain`),
  KEY `title` (`title`),
  KEY `author` (`author`),
  KEY `status` (`status`),
  KEY `application` (`application`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `system_applications`
--
ALTER TABLE `system_applications`
  ADD CONSTRAINT `system_applications_ibfk_2` FOREIGN KEY (`author`) REFERENCES `system_accounts` (`username`) ON UPDATE CASCADE,
  ADD CONSTRAINT `system_applications_ibfk_1` FOREIGN KEY (`type`) REFERENCES `system_applications_types` (`type`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;