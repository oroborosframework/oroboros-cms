<?php

namespace oroboros\src\system\library\libraries\validation;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of InstallationValidator
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
final class InstallationValidator extends \oroboros\src\lib\common\abstracts\libraries\validation\AbstractValidator {

    private $_workers = array(
        'account' => 'workers\\install\\FormValidator',
        'database' => 'workers\\install\\DatabaseValidator',
        'dns' => 'workers\\install\\DnsValidator',
        'deployment' => 'workers\\install\\DeploymentValidator',
        'license' => 'workers\\install\\LicenseValidator',
        'versioncontrol' => 'workers\\install\\VersionControlValidator',
        'recovery' => 'workers\\install\\RecoveryValidator',
    );

    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
        $this->_registerWorkers($this->_workers);
    }

    final public function validate($data, array $schema) {
        try {
            $this->_checkInitialiation();
            $result = parent::validate($data, $schema);
            return $result;
        } catch (\Exception $e) {
            //Expected error
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Validation could not continue due to an error: " . $e->getMessage(), self::ERROR_LOGIC_BAD_PARAMETERS, $e);
        }
    }

    private function _checkMode($mode) {
        return (in_array($mode, $this->_valid_modes));
    }

    private function _onStateChange() {
        $this->_worker = $this->_loadWorker($this->_mode, $this->_key);
    }

    private function _checkInitialiation() {
        if (!$this->_initialized) {
            throw new \OroborosInstallerException("Validator must be initialized before use!", 1601);
        }
    }

    private function _setSchema(\stdClass $schema) {
        $this->_schema = $schema;
    }

    private function _loadWorker($name, $key = NULL) {
        $directory = OROBOROS_INSTALL_RESOURCES . 'installer' . DIRECTORY_SEPARATOR . 'workers' . DIRECTORY_SEPARATOR;
        $file = $directory . strtolower((string) $name) . '.php';
        $class = "\\OroborosInstallationWorker" . ucfirst((string) $name);
        if (!is_readable($file)) {
            throw new \OroborosInstallerException("[ERROR] Unknown validation worker: " . strtolower((string) $name) . "!", 1655);
        }
        if (!in_array($file, get_included_files())) {
            require_once $file;
        }
        $instance = new $class();
        $instance->initialize($this->_schema);
        return $instance;
    }

}
