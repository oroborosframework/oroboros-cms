<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros;

/**
 * <Oroboros Common Library>
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @final
 * @since 0.0.2a
 */
final class Common {

    const FLAG_RECURSIVE = '::recursive::';

    /**
     * <Array Each Method>
     * Applies a callback as a filter to each element of the subject.
     * The callback should expect the first parameter (the value) to 
     * be passed by reference, and the second parameter (the key) to 
     * be passed normally.
     * 
     * The callback parameter may be any of the following:
     * -any calleable argument
     * -an object with a public method called 'execute'
     * -an array, with an object as the first parameter and a public method as the second parameter
     * @param array $subject
     * @param mixed $callback
     * @return boolean [true] if execution occurred, [false] if the callback was invalid.
     * @see \oroboros\Common::map() for passback without referential changes
     * @since 0.0.2a
     */
    static function each(array &$subject, $callback) {
        $result = FALSE;
        if (!is_callable($callback) &&
                !((is_array($callback) && isset($callback[0]) && isset($callback[1]) && is_object($callback[0]) && is_string($callback[1]) && is_callable($callback[0]->{$callback[1]})
                ) || (is_object($callback) && method_exists($callback, 'execute') && is_callable($callback->execute)
                ) || ((is_object($callback)) && method_exists($callback, 'execute') && is_callable($callback::execute)
                ))) {
            return FALSE;
        }
        if (is_callable($callback)) {
            $result = array_walk($subject, $callback);
        } elseif (is_array($callback) && isset($callback[0]) && isset($callback[1]) && is_object($callback[0]) && is_string($callback[1]) && is_callable($callback[0]->{$callback[1]})) {
            //execute callback
            $result = array_walk($callback[0]->{$callback[1]}, $callback);
        } elseif ((is_object($callback) && isset($callback->execute) && is_callable($subject, $callback->execute)
                )) {
            //execute object callback
            $result = array_walk($subject, $callback::execute);
        } elseif (((is_object($callback) || is_string($callback)) && isset($callback->execute) && is_callable($callback::execute)
                )) {
            //execute static callback
            $result = array_walk($subject, $callback::execute);
        }
        return $result;
    }

    /**
     * <Array Map Method>
     * Applies a callback as a filter to each element of the subject.
     * The callback should expect the first parameter (the value) and 
     * the second parameter (the key) to be passed normally, and should 
     * return a new result. Does not change original subject. Unlike 
     * native PHP array_map, this follows the same argument pattern 
     * as array walk, and passes the key as the second argument.
     *  
     * The callback parameter may be any of the following:
     * -any calleable argument
     * -an object with a public method called 'execute'
     * -an array, with an object as the first parameter and a public method as the second parameter
     * @param array $subject
     * @param mixed $callback
     * @return array|boolean The results of the callback, or [false] if the callback was invalid.
     * @see \oroboros\Common::each() for referential changes
     * @since 0.0.2a
     */
    static function map(array $subject, $callback) {
        $result = array();
        if (!is_callable($callback) &&
                !((is_array($callback) && isset($callback[0]) && isset($callback[1]) && is_object($callback[0]) && is_string($callback[1]) && is_callable($callback[0]->{$callback[1]})
                ) || (is_object($callback) && method_exists($callback, 'execute') && is_callable($callback->execute)
                ) || ((is_object($callback)) && method_exists($callback, 'execute') && is_callable($callback::execute)
                ))) {
            return FALSE;
        }
        foreach ($subject as $key => $value) {
            if (is_callable($callback)) {
                $result[$key] = $callback($value, $key);
            } elseif (is_array($callback) && isset($callback[0]) && isset($callback[1]) && is_object($callback[0]) && is_string($callback[1]) && is_callable($callback[0]->{$callback[1]})) {
                //execute callback
                $result[$key] = $callback[0]->{$callback[1]}($value, $key);
            } elseif ((is_object($callback) && isset($callback->execute) && is_callable($subject, $callback->execute)
                    )) {
                //execute object callback
                $result[$key] = $callback->execute($value, $key);
            } elseif (((is_object($callback) || is_string($callback)) && isset($callback->execute) && is_callable($callback::execute)
                    )) {
                //execute static callback
                $result[$key] = $callback::execute($value, $key);
            }
        }
        return $result;
    }

    /**
     * <Cast Data to Type Method>
     * Casts the supplied data [$data] to the type specified by [$type]. 
     * Will use [$delimiter] for implode/explode operations.
     * @param string $type [string|array|object|int|bool]
     * @param mixed $data The data to cast
     * @param type $delimiter (optional) A delimiter for implode/explode operations, when casting strings to objects or arrays.
     * @param array $flags (optional) Accepts [ ::recursive:: ] (unimplemented)
     * @return mixed Returns [$data] cast to the type specified by [$type], or NULL if not possible.
     * @since 0.0.2a
     */
    static function cast($type, $data, $delimiter = ' ', array $flags = array()) {
        switch ($type) {
            case 'string':
                $result = (is_string($data) ? $data : ((is_array($data)) ? implode($delimiter, $data) : ((is_int($data) || (is_bool($data)) || (is_object($data))) ? (string) $data : NULL ) ) );
                break;
            case 'array':
                $result = (is_string($data) ? explode($delimiter, $data) : ((is_array($data) || (is_object($data))) ? json_decode(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), JSON_OBJECT_AS_ARRAY) : ((is_int($data)) ? array($data) : ((is_bool($data)) ? (array) $data : FALSE) ) ) );
                break;
            case 'object':
                $result = (is_string($data) ? json_decode(json_encode(explode($delimiter, $data), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)) : ((is_array($data)) ? json_decode(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)) : ((is_object($data)) ? $data : NULL) ) );
                break;
            case 'int':
                $result = (is_string($data) ? (int) $data : ((is_int($data)) ? $data : ((is_bool($data)) ? (($data) ? 1 : 0) : NULL) ) );
                break;
            case 'bool':
                $result = (is_string($data) ? (($data) ? 'true' : 'false') : ((is_array($data)) ? ((empty($data)) ? FALSE : TRUE) : ((is_object($data)) ? TRUE : ((is_int($data)) ? (($data) ? TRUE : FALSE) : ((is_bool($data)) ? $data : ((is_null($data)) ? 'null' : NULL)
                                                        ) ) ) ) );
                break;
            default:
                return NULL;
                break;
        }
        return $result;
    }
    
    /**
     * <Check Internet Connection>
     * This method is meant to help prevent 
     * requests if the system is running on 
     * a dev machine without an internet 
     * connection. This makes a safe check 
     * to determine whether to serve resources 
     * from a CDN or locally.
     * @return boolean [true] if connected to the internet, [false] if no network connection available.
     * @note The site used here [example.com] is maintained by IANA, and should never be down unless the entire internet is down, which should circumvent any false flags.
     * @since 0.0.2a
     */
    static function isInternetAccessible() {
    $connected = @fsockopen("example.com", 80); 
    if ($connected){
        $result = TRUE;
        fclose($connected);
    }else{
        $result = FALSE;
    }
    return $result;

}

}
